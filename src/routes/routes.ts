import { Request, Response } from "express";
import { ImagesController } from '../controllers/images.controller';
import { body } from 'express-validator/check';

export class Routes {
    public imagesController: ImagesController = new ImagesController();
    public routes(app: any): void {
        // app.route("/").get(this.nodesController.index);
        app
            .route("/images")
            .get(this.imagesController.index)
            .post([
                body('imageTitle').trim().not().isEmpty()
            ], this.imagesController.create)
        app
            .route("/images/:id")
            .get(this.imagesController.show)
            .put(this.imagesController.update)
            .delete(this.imagesController.delete);
    }
}