import dotenv from 'dotenv';
import App from './app';
import BaseRoute from './routes/baseRoute';
import * as connectDb from './utils/dbConnect';
import createDb from './utils/dbCreateDb';

dotenv.config()
const app = new App()
createDb().then(()=>{
    connectDb.db.sequelize.sync()
    .then(() => {
        app.listen()
        console.log('server started process: ', process.env.PORT);
    }).catch((err)=> {
        console.log('err', err);
        throw err;
    });
});

