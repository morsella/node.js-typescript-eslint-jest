export interface ImageInterface {
    title?: string;
    imageUrl: string;
    eTag: string;
}