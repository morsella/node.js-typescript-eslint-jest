import { Request, Response, NextFunction } from 'express';
export default function headersMiddleware(req: Request, res: Response, next: NextFunction): void {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PUT, PATCH, DELETE'
      );
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, x-xsrf-token, cart-session, session');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    // if (req.method === 'OPTIONS') {
    //     return res.sendStatus(200);
    //   }
    next();
}