import express, { request, response, Request, Response } from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import compression from 'compression';
import fileUpload from 'express-fileupload';
// import bcrypt from 'bcryptjs';
// import session from 'express-session';
// import cookieParser from 'cookie-parser';
import errorMiddleware from './middlewares/errorMiddleware';
import headersMiddleware from './middlewares/headersMiddleware';
import { Routes } from "./routes/routes";

class App { 
  app: express.Application;
  public appRoutes: Routes = new Routes();
  constructor() {
    this.app = express();
    this.initMiddlewares();
    this.initHeaders();
    this.appRoutes.routes(this.app);
    this.initErrorHandler();
  }

  listen(): void{
    const port  = process.env.PORT;
    this.app.listen(port, () => {
      console.log(`App listening on the port ${port}`);
    });
  }
  private initMiddlewares(): void {
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(bodyParser.urlencoded({ extended: false })); // x-www-form-urlencoded
    this.app.use(bodyParser.json()); // application/json
    this.app.use(fileUpload());
  }
  private initHeaders(): void{
    this.app.use(headersMiddleware);
  }
  private initErrorHandler(): void {
    this.app.use(errorMiddleware);
  }
}
 
export default App;


