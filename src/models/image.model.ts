import { Model, DataTypes } from "sequelize";
import { db } from "../utils/dbConnect";

export class Image extends Model {
  public id!: number;
  public title!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public imageUrl!: string;
}

Image.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
      },
      title: {
        type: new DataTypes.STRING(128),
        allowNull: false
      },
      imageUrl: {
        type: new DataTypes.STRING,
        allowNull: false
      }
    },
    {
      tableName: "images",
      sequelize: db.sequelize
    }
  );
  
  // Image.sync({ force: true }).then(() => console.log("Images table created"));
  Image.sync().then(() => console.log("Images table created"));