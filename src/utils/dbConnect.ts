import { Sequelize } from "sequelize";
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.ts')[env];
const sequelize = new Sequelize (config.database, config.username, config.password, {
    dialect: config.dialect,
    host: config.host
});

export const db = {
    sequelize: sequelize,
    Sequelize: Sequelize
} 