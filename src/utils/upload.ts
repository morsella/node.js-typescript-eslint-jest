import dotenv from 'dotenv';
import AWS from 'aws-sdk';
import { ImageInterface } from '../interfaces/image.interface'
import { ErrorInterface } from  '../interfaces/error.interface'

dotenv.config()
//configure the keys for accessing AWS
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});
const s3Client = new AWS.S3();
const dateNow = new Date().getTime()+'-';

const upload = (file: any) : Promise<ImageInterface> =>{
// Return new promise 
return new Promise(function(resolve, reject){
  // Do async job
  s3Client.putObject({
    Bucket: process.env.S3_BUCKET || '',
    Key: dateNow+file.name,
    Body: file.data
  }, function (err: AWS.AWSError, resp: AWS.S3.PutObjectOutput) {
    if (err) { 
      reject (err); 
    } 
    else {
      if(resp.ETag && resp.ETag.trim().length > 0){
          // let result = { eTag: resp.ETag, imageUrl: `https://s3.eu-central-1.amazonaws.com/${process.env.S3_BUCKET}/${dateNow+file.name}`};
          let result = { eTag: resp.ETag, imageUrl: `https://recce.moresella.nl/uploads/${dateNow+file.name}`};
          resolve(result);
      }
      else{
        reject({message: 'There was a problem saving file'});
      }
    }
  });
 });
}
export default upload;