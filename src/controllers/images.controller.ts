import { Request, Response, NextFunction } from "express"
import { Image } from "../models/image.model"
import { ImageInterface } from '../interfaces/image.interface'
import { UpdateOptions, DestroyOptions } from "sequelize"
import upload from '../utils/upload'
import { UploadedFile } from "express-fileupload"
import { imagesDTO } from '../dto/imageDto'

export class ImagesController {
  public index(req: Request, res: Response, next: NextFunction) {
    Image.findAll<Image>({})
      .then((images: Array<Image>) => res.json({images: imagesDTO(images)}))
      .catch((err: Error) => next(err));
  }

  public create = async(req: Request, res: Response, next: NextFunction) => {
    try{
      console.log('files',  req.files)
      console.log('body', req.body)
        if(req.files && req.files.image){
            let file = req.files.image as UploadedFile;
            if (
                file.mimetype === 'image/png' ||
                file.mimetype === 'image/jpg' ||
                file.mimetype === 'image/jpeg'
              ) {
                const uploadToS3 = await upload(file);
                console.log('uploaded', uploadToS3);
                
                if(uploadToS3 && uploadToS3.eTag){
                    const params: ImageInterface = {
                        title: req.body.imageTitle,
                        imageUrl: uploadToS3.imageUrl,
                        eTag: uploadToS3.eTag
                    };
                   const saveImage = await Image.create<Image>(params)
                    // .then((image: Image) => res.status(201).json(image))
                    // .catch((err: Error) => next(err));
                  console.log(saveImage);
                    res.status(201).json({
                        'imagUrl': saveImage.imageUrl
                    });
                }
                // else {
                //     const error = new Error('upload failed');
                //     if(!error.statusCode) error.statusCode = 422;
                //     throw error;
                // }
              } 
              // else {
              //   const error = new Error('Image mime type is invalid');
              //   if(!error.statusCode) error.statusCode = 422;
              //   throw error;
              // }
        }
    }
    catch(err){
        if (!err.statusCode) {
            err.statusCode = 500;
          }
          next(err);
    }
  }

  public show(req: Request, res: Response, next: NextFunction) {
    const imageId: number = +req.params.id;
    Image.findByPk<Image>(imageId)
      .then((node: Image | null) => {
        if (node) {
          res.json(node);
        } 
        else {
          res.status(404).json({ errors: ["Node not found"] });
        }
      })
      .catch((err: Error) => next(err));
  }

  public update(req: Request, res: Response, next: NextFunction) {
    const imageId: number = +req.params.id;
    const params: ImageInterface = req.body;

    const update: UpdateOptions = {
      where: { id: imageId },
      limit: 1
    };

    Image.update(params, update)
      .then(() => res.status(202).json({ data: "success" }))
      .catch((err: Error) => next(err));
  }

  public delete(req: Request, res: Response, next: NextFunction) {
    const imageId: number = +req.params.id;
    const options: DestroyOptions = {
      where: { id: imageId },
      limit: 1
    };

    Image.destroy(options)
      .then(() => res.status(204).json({ data: "success" }))
      .catch((err: Error) => next(err));
  }
}

//error response as res.status....