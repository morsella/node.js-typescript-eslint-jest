import { Image } from '../models/image.model'

export const imagesDTO = (images: Image[]) => {
   const imageDto = images.map(i => {
       return { 
        title: i.title,
        imageUrl: i.imageUrl
       }
    })
    return imageDto;
}